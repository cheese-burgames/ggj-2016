﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	public UnityEvent SetupDone;

	public void QuitGame() {
		Application.Quit();
	}

	public void LoadGame() {
		SceneManager.LoadScene("Gameplay");
	}

	public void UpdateSetupStatus() {
		if (InputConfigurator.InputTypeForPlayer(PlayerName.Player1) != InputType.Unknown && InputConfigurator.InputTypeForPlayer(PlayerName.Player2) != InputType.Unknown)
			SetupDone.Invoke();
	}

}
