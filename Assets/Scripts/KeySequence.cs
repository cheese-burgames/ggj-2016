﻿using UnityEngine;
using System.Collections;

public class KeySequence
{
	public static KeyName[] PREACHING_KEYS = new KeyName[]{ KeyName.Ritual1, KeyName.Ritual2, KeyName.Ritual3, KeyName.Ritual4 };

	private KeyName[] keys;
	private int currentKey;

	public KeySequence(int length) {
		keys = GenerateRandomKeySequence(length);
		currentKey = 0;
	}

	static KeyName[] GenerateRandomKeySequence(int length) {
		KeyName[] keys = new KeyName[length];
		for (int i = 0; i < length; i++)
			keys[i] = PREACHING_KEYS[Random.Range(0, PREACHING_KEYS.Length)];
		return keys;
	}

	public bool IsKeyValid(KeyName name) {
		if (name == keys[currentKey]) {
			currentKey++;
			return true;
		}
		else
			return false;
	}

	public int CurrentKeyIndex {
		get { return currentKey; }
	}

	public bool Complete {
		get { return (currentKey >= keys.Length); }
	}

	public int Length {
		get { return this.keys.Length; }
	}

	public KeyName this[int index] {
		get { return keys[index]; }
	}
}

