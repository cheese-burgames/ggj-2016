﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[RequireComponent(typeof(CircleCollider2D), typeof(Rigidbody2D))]
public class Peon : MapIndividual {

	private struct DestinationStatusResult {
		public DestinationStatus status;
		public Collider2D collider;

		public DestinationStatusResult (DestinationStatus status, Collider2D collider = null) {
			this.status = status;
			this.collider = collider;
		}
	}

	private enum DestinationStatus {
		Valid, OutOfMap, CollidingWithOther, CollidingWithObstacle
	}

	[Header("Movement")]
	public float speed = 1;
	public float lookupAngle = 5;
	public float lookupAngleOutOfMap = 90;

	[Header("Obstacles")]
	public LayerMask obstaclesLayerMask;

	[Header("Animation")]
	public RuntimeAnimatorController monsterAnimationController;

	[Header("Tints")]
	public Color player1Tint = Color.white;
	public Color player2Tint = Color.white;

	protected Vector2 direction;
	private LayerMask layerMask;

	protected static Collider2D[] physicsResult = new Collider2D[1];
	protected static RaycastHit2D[] raycastResult = new RaycastHit2D[1];
	private Rect peonZone;

	private Player[] fleeingPlayers;
	private float[] fleeingDurations;
	private Vector2[] fleeingDirections;

	protected bool paused = false;

	protected bool listening = false;
	protected Vector2 listeningDirection;

	public bool Converted { get; private set; }
	public Player ConvertingPlayer { get; private set; }
	private Vector2 conversionDestination;

	private bool captured = false;
	public bool Captured {
		get { return captured; }
		private set {
			captured = true;
			ConvertingPlayer.score++;
		} 
	}

	new public CircleCollider2D collider {
		get { return GetComponent<CircleCollider2D>(); }
	}

	virtual protected void Awake() {
		peonZone = FindObjectOfType<GameplayManager>().peonStartZone;

		fleeingPlayers = FindObjectsOfType<Player>();
		fleeingDurations = new float[fleeingPlayers.Length];
		Array.Clear(fleeingDurations, 0, fleeingDurations.Length);
		fleeingDirections = new Vector2[fleeingPlayers.Length];

		layerMask = LayerMask.GetMask(LayerMask.LayerToName(gameObject.layer));
	}

	// Use this for initialization
	virtual protected void Start () {
		direction = Random.insideUnitCircle.normalized;
	}
	
	// Update is called once per frame
	virtual protected void FixedUpdate () {
		if (paused || listening || Captured)
			return;

		Rigidbody2D body = GetComponent<Rigidbody2D>();
		float multiplier = speed * Time.fixedDeltaTime;

		if (Converted) {
			float destinationDistance = Vector2.Distance(conversionDestination, body.position);
			if (destinationDistance < multiplier) {
				body.MovePosition(conversionDestination);
				direction = Vector2.down;
				GetComponent<Animator>().SetBool("Captured", true);
				Captured = true;
				return;
			}

			Vector2 targetDirection = (conversionDestination - body.position).normalized;
			Vector3 cross = Vector3.Cross(direction, targetDirection);
			float angle = Vector2.Angle(direction, targetDirection);
			direction = Quaternion.AngleAxis(Mathf.Min(5, angle), cross) * direction;

			Vector3 newPos = (Vector3) (body.position + direction * multiplier);

			collider.enabled = false;
			bool found = false;
			float angleForMaxDistance = 0;
			if (Physics2D.OverlapPointNonAlloc(newPos, physicsResult, obstaclesLayerMask) > 0) {
				float f = lookupAngle;
				float maxDistance = float.MinValue;
				while (f < 180) {
					Vector2 newDir = Quaternion.AngleAxis(f, Vector3.forward) * direction;
					newPos = (Vector3) (body.position + newDir * multiplier);
					Vector2 end = (Vector2) newPos + newDir * 10000;
					if (Physics2D.LinecastNonAlloc(newPos, end, raycastResult, obstaclesLayerMask) == 0) {
						found = true;
						direction = newDir;
						break;
					}
					else {
						if (maxDistance < raycastResult[0].distance) {
							maxDistance = raycastResult[0].distance;
							angleForMaxDistance = f;
						}
					}

					newDir = Quaternion.AngleAxis(-f, Vector3.forward) * direction;
					newPos = (Vector3) (body.position + newDir * multiplier);
					end = (Vector2) newPos + newDir * 10000;
					if (Physics2D.LinecastNonAlloc(newPos, end, raycastResult, obstaclesLayerMask) == 0) {
						found = true;
						direction = newDir;
						break;
					}
					else {
						if (maxDistance < raycastResult[0].distance) {
							maxDistance = raycastResult[0].distance;
							angleForMaxDistance = f;
						}
					}

					f += lookupAngle;
				}

				if (!found) {
					direction = Quaternion.AngleAxis(angleForMaxDistance, Vector3.forward) * direction;
					newPos = (Vector3) (body.position + direction * multiplier);
					found = true;
				}
			}
			else
				found = true;

			if (found)
				body.MovePosition(newPos);
			collider.enabled = true;
		}
		else {
			// Change course direction to flee players
			bool hasFleeingPlayer = false;
			for (int i = 0; i < fleeingPlayers.Length; i++) {
				if (fleeingDurations[i] > 0) {
					fleeingDirections[i] = fleeingPlayers[i].transform.position - (Vector3) body.position;
					fleeingDurations[i] = Mathf.Max(0, fleeingDurations[i] - Time.fixedDeltaTime);
					hasFleeingPlayer = true;
				}
				else
					fleeingDirections[i] = Vector2.zero;
			}
			if (hasFleeingPlayer) {
				Vector2 fleeingDirection = Vector2.zero;
				for (int i = 0; i < fleeingDirections.Length; i++)
					fleeingDirection += fleeingDirections[i];
				fleeingDirection.Normalize();

				direction = -fleeingDirection;
			}

			Vector3 newPos = (Vector3) (body.position + direction * multiplier);

			collider.enabled = false;
			bool found = false;
			DestinationStatusResult result = IsDestinationValid(newPos);
			switch (result.status) {
			case DestinationStatus.Valid:
				found = true;
				break;
			case DestinationStatus.OutOfMap:
				if (newPos.y < peonZone.min.y || newPos.y > peonZone.max.y)
					direction.y *= -1;
				if (newPos.x < peonZone.min.x || newPos.x > peonZone.max.x)
					direction.x *= -1;
				break;
			case DestinationStatus.CollidingWithObstacle:
				Bounds b = result.collider.bounds;
				if (b.Contains(newPos)) {
					Vector2 diff = newPos - b.center;
					if (Mathf.Abs(diff.x) > Mathf.Abs(diff.y))
						direction.x *= -1;
					else
						direction.y *= -1;
				}
				else {
					if (newPos.y < b.min.y || newPos.y > b.max.y)
						direction.y *= -1;
					if (newPos.x < b.min.x || newPos.x > b.max.x)
						direction.x *= -1;
				}
				break;
			case DestinationStatus.CollidingWithOther:
				float f = lookupAngle;
				while (f < 180) {
					Vector2 newDir = Quaternion.AngleAxis(f, Vector3.forward) * direction;
					newPos = (Vector3) (body.position + newDir * multiplier);
					if (IsDestinationValid(newPos).status != DestinationStatus.Valid) {
						found = true;
						direction = newDir;
						break;
					}

					newDir = Quaternion.AngleAxis(-f, Vector3.forward) * direction;
					newPos = (Vector3) (body.position + newDir * multiplier);
					if (IsDestinationValid(newPos).status != DestinationStatus.Valid) {
						found = true;
						direction = newDir;
						break;
					}

					f += lookupAngle;
				}
				break;
			}

			if (found)
				body.MovePosition(newPos);
			collider.enabled = true;
		}
	}

	virtual protected void Update() {
		if (!Captured) {
			Vector2 animDirection = listening ? listeningDirection : direction;

			GetComponent<SpriteRenderer>().flipX = (animDirection.x > 0);

			Animator anim = GetComponent<Animator>();
			if (Mathf.Abs(animDirection.x) > Mathf.Abs(animDirection.y))
				anim.SetInteger("Direction", 0);
			else if (animDirection.y > 0)
				anim.SetInteger("Direction", 2);
			else
				anim.SetInteger("Direction", 1);

			anim.SetBool("Paused", paused || listening);
		}
	}

	DestinationStatusResult IsDestinationValid(Vector2 destination) {
		if (!peonZone.Contains(destination))
			return new DestinationStatusResult(DestinationStatus.OutOfMap);
		if (Physics2D.OverlapCircleNonAlloc(destination, collider.radius, physicsResult, obstaclesLayerMask) > 0)
			return new DestinationStatusResult(DestinationStatus.CollidingWithObstacle, physicsResult[0]);
		if (Physics2D.OverlapCircleNonAlloc(destination, collider.radius, physicsResult, layerMask) > 0)
			return new DestinationStatusResult(DestinationStatus.CollidingWithOther, physicsResult[0]);
		return new DestinationStatusResult(DestinationStatus.Valid);
	}

	virtual protected void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.DrawLine(Vector3.zero, listening ? listeningDirection : direction);
	}

	public void Flee(Player player, float duration) {
		if (fleeingPlayers != null) {
			int index = Array.IndexOf(fleeingPlayers, player);
			if (index >= 0)
				fleeingDurations[index] = duration;
		}
	}

	public void Listen(Player player) {
		listening = true;
		listeningDirection = (player.transform.position - transform.position).normalized;

		player.PreachEnded += StopListening;
	}

	void StopListening(Player sender, bool success) {
		sender.PreachEnded -= StopListening;

		listening = false;

		if (success) {
			Converted = true;
			ConvertingPlayer = sender;
			ConvertingPlayer.IncreaseRepulsivity();

			GetComponent<Animator>().runtimeAnimatorController = monsterAnimationController;

			conversionDestination = FindObjectOfType<GameplayManager>().GetRandomConversionDestination();

			GetComponent<SpriteRenderer>().color = (ConvertingPlayer.player == PlayerName.Player1) ? player1Tint : player2Tint;
		}

	}

}
