﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[RequireComponent(typeof(CircleCollider2D))]
public class Player : MapIndividual {

	public delegate void PlayerEventHandler(Player sender);
	public delegate void PlayerPreachEventHandler(Player sender, bool success);

	public event PlayerPreachEventHandler PreachEnded;

	public PlayerName player;

	[Header("Movement")]
	public float speed;
	public CircleCollider2D movementCollider;
	public LayerMask obstaclesLayerMask;

	[Header("Repulsion")]
	[Range(0, 1)] public float minRepulsionProbability = 0;
	[Range(0, 1)] public float maxRepulsionProbability = 1;
	[Range(0,1)] public float repulsionIncrement = 0.05f;
	private float repulsionProbability;
	public float fleeDuration;

	[Header("Preach")]
	public CircleCollider2D captureCollider;
	public RectTransform keyContainer;
	public Image keyImagePrefab;

	[Header("Helpers")]
	public Transform captureZone;

	[Header("Stun")]
	public float stunDuration = 3;

	[Header("Audio")]
	public AudioClip convert;
	public AudioClip convert2;
	public AudioClip stunSound;
	public AudioSource preachingSound;
	public AudioSource[] preachingSoundFailed;
	public AudioSource[] preachingSoundSucceeded;

	private Rect movementZone;
	private List<Collider2D> excludedColliders = new List<Collider2D>();
	private Collider2D[] physicsResult = new Collider2D[1];

	private bool isStun = false;

	[NonSerialized] public int score = 0;

	private bool canPreach = false;
	public bool CanPreach {
		get { return canPreach; }
		private set {
			if (canPreach != value) {
				canPreach = value;
				UpdateKeyContainer();
			}
		}
	}

	void UpdateKeyContainer(bool clear = true) {
		if (clear)
			CleanKeyContainer();

		if (canPreach) {
			Image img = Instantiate<Image>(keyImagePrefab);
			img.sprite = KeyManager.GetKeySpriteForPlayer(KeyName.Preach, player);
			img.SetNativeSize();
			img.transform.SetParent(keyContainer, false);
		}
		else if (currentPreachingSequence != null) {
			if (clear) {
				for (int i = 0; i < currentPreachingSequence.Length; i++) {
					Image img = Instantiate<Image>(keyImagePrefab);
					img.sprite = KeyManager.GetKeySpriteForPlayer(currentPreachingSequence[i], player);
					img.SetNativeSize();
					img.transform.SetParent(keyContainer, false);
				}
			}
			else {
				for (int i = 0; i < currentPreachingSequence.CurrentKeyIndex; i++) {
					Image img = keyContainer.GetChild(i).GetComponent<Image>();
					img.color = Color.gray;
				}
			}
		}
	}

	void CleanKeyContainer() {
		for (int i = keyContainer.childCount - 1; i >= 0; i--)
			Destroy(keyContainer.GetChild(i).gameObject);
	}

	private KeySequence currentPreachingSequence;

	void Start() {
		repulsionProbability = minRepulsionProbability;
		movementZone = FindObjectOfType<GameplayManager>().peonStartZone;

		captureZone.localScale = Vector3.one * captureCollider.radius * 2;
	}

	void Update () {
		if (isStun)
			return;

		if (!IsPreaching) {
			Vector3 direction = new Vector3 (player.GetAxis (RectTransform.Axis.Horizontal), player.GetAxis (RectTransform.Axis.Vertical));
			Vector3 move = transform.position + direction * speed * Time.deltaTime;
			move.x = Mathf.Clamp (move.x, movementZone.min.x, movementZone.max.x);
			move.y = Mathf.Clamp (move.y, movementZone.min.y, movementZone.max.y);

			bool isMoving = (direction.magnitude > 0);
			if (Physics2D.OverlapCircleNonAlloc (move, movementCollider.radius, physicsResult, obstaclesLayerMask) == 0)
				transform.position = move;
			else
				isMoving = false;

			GetComponent<SpriteRenderer>().flipX = (direction.x > 0);

			DetectPreach();
			if (player.GetKeyUp (KeyName.Preach))
				Preach ();

			Animator anim = GetComponent<Animator> ();
			anim.SetBool("Preaching", IsPreaching);
			if (isMoving) {
				anim.SetFloat("hAbsVelocity", Mathf.Abs(direction.x));
				anim.SetFloat("vVelocity", direction.y);
			}
			else {
				anim.SetFloat("hAbsVelocity", 0);
				anim.SetFloat("vVelocity", 0);
			}
		}
		else {
			foreach (KeyName keyName in KeySequence.PREACHING_KEYS) {
				if (player.GetKeyUp(keyName)) {
					if (currentPreachingSequence.IsKeyValid(keyName)) {
						UpdateKeyContainer(false);
						if (currentPreachingSequence.Complete)
							EndPreach(true);
					}
					else
						EndPreach(false);
				}
			}
		}
	}

	void DetectPreach() {
		bool cp = false;
		Peon[] peons = FindObjectsOfType<Peon>();
		foreach (Peon p in peons) {
			if (!p.Captured || (p.Converted && p.ConvertingPlayer != this)) {
				float distance = Vector2.Distance(p.transform.position, transform.position);
				if (distance < captureCollider.radius) {
					cp = true;
					break;
				}
			}
		}
		CanPreach = cp;
	}

	void Preach() {
		CanPreach = false;

		int peonCount = 0;
		bool hasPeonConvertedByEnemy = false;
		Peon[] peons = FindObjectsOfType<Peon>();
		foreach (Peon p in peons) {
			if (!p.Captured || (p.Converted && p.ConvertingPlayer != this)) {
				float distance = Vector2.Distance(p.transform.position, transform.position);
				if (distance < captureCollider.radius) {
					p.Listen(this);
					peonCount++;
					hasPeonConvertedByEnemy = hasPeonConvertedByEnemy || p.Captured;
				}
			}
		}

		if (peonCount > 0) {
			IsPreaching = true;
			preachingSound.Play();

			int length = 4;
			if (hasPeonConvertedByEnemy || peonCount > 3)
				length = 6;
			else if (peonCount > 1)
				length = 5;
			currentPreachingSequence = new KeySequence(length);
			UpdateKeyContainer();

			GetComponent<Animator>().SetBool ("Preaching", true);
		}
	}

	public void EndPreach(bool success) {
		IsPreaching = false;
		preachingSound.Stop();

		currentPreachingSequence = null;
		UpdateKeyContainer();

		if (!success) {
			Stun(false);

			foreach (AudioSource source in preachingSoundFailed)
				source.Play();
		}
		else {
			AudioSource.PlayClipAtPoint(convert, Camera.main.transform.position);
			Invoke("PlayConvert2", 1.5f);

			foreach (AudioSource source in preachingSoundSucceeded)
				source.Play();
		}

		if (PreachEnded != null)
			PreachEnded(this, success);
	}

	public bool IsPreaching { get; private set; }

	void PlayConvert2() {
		AudioSource.PlayClipAtPoint(convert2, Camera.main.transform.position);
	}

	public void Stun(bool sound = true) {
		isStun = true;
		GetComponent<Animator>().SetBool("Stun", true);
		Invoke("EndStun", stunDuration);
		AudioSource.PlayClipAtPoint(stunSound, Camera.main.transform.position);
	}

	void EndStun() {
		isStun = false;
		GetComponent<Animator>().SetBool("Stun", false);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (!excludedColliders.Contains(other)) {
			Sentinel s = other.GetComponent<Sentinel>();
			if (s == null || !s.IsTracking) {
				Peon p = other.GetComponent<Peon>();
				if (p != null) {
					if (Random.value < repulsionProbability)
						p.Flee(this, fleeDuration);
					excludedColliders.Add(other);
				}
			}
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		excludedColliders.Remove(other);
	}

	public void IncreaseRepulsivity() {
		repulsionProbability = Mathf.Min(maxRepulsionProbability, repulsionProbability + repulsionIncrement);
	}

}
