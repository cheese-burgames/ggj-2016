﻿using UnityEngine;
using System.Collections.Generic;

public static class InputConfigurator
{
	private static Dictionary<PlayerName, InputType> inputTypes = new Dictionary<PlayerName, InputType>();

	public static void SetInputTypeForPlayer(PlayerName player, InputType type) {
		if (type != InputType.Unknown)
			inputTypes[player] = type;
	}

	public static InputType InputTypeForPlayer(PlayerName player) {
		return inputTypes.ContainsKey(player) ? inputTypes[player] : InputType.Unknown;
	}

	public static KeyCode KeyForPlayer(PlayerName player, KeyName key) {
		InputType type = InputTypeForPlayer(player);
		bool isPlayer1 = (player == PlayerName.Player1);

		if (type != InputType.Unknown) {
			switch (key) {
			case KeyName.Left:
				if (!type.IsPad()) {
					if (isPlayer1)
						return (type == InputType.KeyboardAzerty) ? KeyCode.Q : KeyCode.A;
					else
						return KeyCode.LeftArrow;
				}
				break;

			case KeyName.Right:
				if (!type.IsPad())
					return isPlayer1 ? KeyCode.D : KeyCode.RightArrow;
				break;

			case KeyName.Up:
				if (!type.IsPad()) {
					if (isPlayer1)
						return (type == InputType.KeyboardAzerty) ? KeyCode.Z : KeyCode.W;
					else
						return KeyCode.UpArrow;
				}
				break;

			case KeyName.Down:
				if (!type.IsPad())
					return isPlayer1 ? KeyCode.S : KeyCode.DownArrow;
				break;

			case KeyName.Preach:
				switch (type) {
				case InputType.KeyboardAzerty:
				case InputType.KeyboardQwerty:
					return isPlayer1 ? KeyCode.C : KeyCode.H;
				case InputType.PadPlaystation:
					return isPlayer1 ? KeyCode.Joystick1Button9 : KeyCode.Joystick2Button9;
				case InputType.PadXbox:
					return isPlayer1 ? KeyCode.Joystick1Button10 : KeyCode.Joystick2Button10;
				}
				break;

			case KeyName.Ritual1:
				switch (type) {
				case InputType.KeyboardAzerty:
				case InputType.KeyboardQwerty:
					return isPlayer1 ? KeyCode.C : KeyCode.H;
				case InputType.PadPlaystation:
					return isPlayer1 ? KeyCode.Joystick1Button12 : KeyCode.Joystick2Button12;
				case InputType.PadXbox:
					return isPlayer1 ? KeyCode.Joystick1Button3 : KeyCode.Joystick2Button3;
				}
				break;

			case KeyName.Ritual2:
				switch (type) {
				case InputType.KeyboardAzerty:
				case InputType.KeyboardQwerty:
					return isPlayer1 ? KeyCode.V : KeyCode.J;
				case InputType.PadPlaystation:
					return isPlayer1 ? KeyCode.Joystick1Button15 : KeyCode.Joystick2Button15;
				case InputType.PadXbox:
					return isPlayer1 ? KeyCode.Joystick1Button2 : KeyCode.Joystick2Button2;
				}
				break;

			case KeyName.Ritual3:
				switch (type) {
				case InputType.KeyboardAzerty:
				case InputType.KeyboardQwerty:
					return isPlayer1 ? KeyCode.B : KeyCode.K;
				case InputType.PadPlaystation:
					return isPlayer1 ? KeyCode.Joystick1Button14 : KeyCode.Joystick2Button14;
				case InputType.PadXbox:
					return isPlayer1 ? KeyCode.Joystick1Button0 : KeyCode.Joystick2Button0;
				}
				break;

			case KeyName.Ritual4:
				switch (type) {
				case InputType.KeyboardAzerty:
				case InputType.KeyboardQwerty:
					return isPlayer1 ? KeyCode.N : KeyCode.L;
				case InputType.PadPlaystation:
					return isPlayer1 ? KeyCode.Joystick1Button13 : KeyCode.Joystick2Button13;
				case InputType.PadXbox:
					return isPlayer1 ? KeyCode.Joystick1Button1 : KeyCode.Joystick2Button1;
				}
				break;
			}
		}

		return KeyCode.None;
	}
}
