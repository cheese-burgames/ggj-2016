﻿using UnityEngine;
using System.Collections;

public class CenterPieceShadow : MonoBehaviour {

	public float gameDuration = 240.0f;

	// Use this for initialization
	void Start () {
		GetComponent<Animator>().speed = 1.0f / gameDuration;
	}

	public void EndCallback() {
		GetComponent<Animator>().speed = 0;
		FindObjectOfType<GameplayManager>().EndGame(EndGameCause.Timeout);
	}

}
