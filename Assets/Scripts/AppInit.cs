﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class AppInit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		Resolution[] resolutions = Screen.resolutions;
		int index = -1;
		int maxPixelCount = int.MinValue;
		for (int i = 0; i < resolutions.Length; i++) {
			int pc = resolutions[i].width * resolutions[i].height;
			if (pc > maxPixelCount) {
				index = i;
				maxPixelCount = pc;
			}
		}
		Screen.SetResolution(resolutions[index].width, resolutions[index].height, true, resolutions[index].refreshRate);

		Invoke("LoadNextScene", 5);
	}

	void LoadNextScene() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

}
