﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ControlImage : MonoBehaviour, IPointerClickHandler {

	public InputType inputType;
	public PlayerName player;

	public Sprite enabledSprite;
	private Sprite originalSprite;

	public UnityEvent onClick;

	void Awake() {
		originalSprite = GetComponent<Image>().sprite;
	}

	public bool Selected {
		set {
			if (value) {
				GetComponent<Image>().sprite = enabledSprite;
				InputConfigurator.SetInputTypeForPlayer(player, inputType);
			}
			else
				GetComponent<Image>().sprite = originalSprite;
		}
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		ControlImage[] controls = transform.parent.GetComponentsInChildren<ControlImage>();
		foreach (ControlImage ci in controls)
			ci.Selected = false;

		Selected = true;

		onClick.Invoke();
	}

}
