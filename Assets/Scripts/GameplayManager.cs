﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using System;
using System.Collections.Generic;

public class GameplayManager : MonoBehaviour, IComparer<SortableElement> {

	[Header("Peons")]
	public Peon peonPrefab;
	public int peonStartCount = 100;
	public Rect peonStartZone;
	public Rect peonStartExclusionZone;
	public Rect peonDestinationZone;
	public LayerMask peonStartLayerMask;
	public Shadow peonShadow;
	public RuntimeAnimatorController alternativeController;

	[Header("Sentinels")]
	public Sentinel sentinelPrefab;
	public int sentinelCount = 5;

	[Header("UI Overlay")]
	public RectTransform overlayContainer;
	public RectTransform player1Panel;
	public RectTransform player2Panel;
	public Image keyImagePrefab;

	[Header("End Game")]
	public Image fadePanel;
	public GameObject timeoutPanel;
	public GameObject winPanel;
	public Text winText;

	[Header("Sounds")]
	public AudioClip sentinelSound;
	private bool sentinelSoundPlaying;

	[Header("Music loops")]
	public AudioSource[] musicLoops;

	private List<SortableElement> sortableElements;
	private Dictionary<PlayerName, RectTransform> playerKeyPanels = new Dictionary<PlayerName, RectTransform>();

	// Use this for initialization
	void Start () {
		Debug.Log("Joysticks:");
		foreach (string name in Input.GetJoystickNames())
			Debug.Log("\t- " + name);

		playerKeyPanels[PlayerName.Player1] = player1Panel;
		playerKeyPanels[PlayerName.Player2] = player2Panel;

		if (InputConfigurator.InputTypeForPlayer(PlayerName.Player1) == InputType.Unknown)
			InputConfigurator.SetInputTypeForPlayer(PlayerName.Player1, InputType.KeyboardAzerty);
		if (InputConfigurator.InputTypeForPlayer(PlayerName.Player2) == InputType.Unknown)
			InputConfigurator.SetInputTypeForPlayer(PlayerName.Player2, InputType.PadPlaystation);

		Collider2D[] result = new Collider2D[1];

		Player[] players = FindObjectsOfType<Player>();
		foreach (Player p in players) {
			Shadow s = Instantiate<Shadow>(peonShadow);
			s.trackedObject = p.transform;
		}

		int peonForSentinel = peonStartCount / sentinelCount;
		sortableElements = new List<SortableElement>(FindObjectsOfType<SortableElement>());
		for (int i = 0; i < peonStartCount; i++) {
			Peon p = ((i % peonForSentinel) == 0) ? Instantiate<Peon>(sentinelPrefab) : Instantiate<Peon>(peonPrefab);

			if (i % 2 == 0)
				p.GetComponent<Animator>().runtimeAnimatorController = alternativeController;

			CircleCollider2D collider = p.GetComponent<CircleCollider2D>();
			collider.enabled = false;

			int c = 0;
			do {
				Vector2 pos = new Vector2(
					Random.Range(peonStartZone.min.x, peonStartZone.max.x),
					Random.Range(peonStartZone.min.y, peonStartZone.max.y)
				);
				if (peonStartExclusionZone.Contains(pos))
					continue;
				p.transform.position = pos;
				c++;
				if (c >= 10)
					break;
			}
			while (Physics2D.OverlapCircleNonAlloc(collider.bounds.center, collider.radius, result, peonStartLayerMask) > 0);

			collider.enabled = true;
			sortableElements.Add(p);

			Shadow s = Instantiate<Shadow>(peonShadow);
			s.trackedObject = p.transform;
		}
	}

	void LateUpdate() {
		sortableElements.Sort(this);

		for (int i = 0; i < sortableElements.Count; i++)
			sortableElements[i].GetComponent<SpriteRenderer>().sortingOrder = i;

		Player[] players = FindObjectsOfType<Player>();
		foreach (Player p in players) {
			RectTransform rt = playerKeyPanels[p.player];
			Vector3 screenPos = Camera.main.WorldToScreenPoint(p.transform.position);
			Vector2 localPos;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(overlayContainer, screenPos, null, out localPos);
			rt.anchoredPosition = localPos;
		}

		Peon[] peons = FindObjectsOfType<Peon>();
		int peonsLeft = 0;
		foreach (Peon p in peons) {
			if (!p.Captured)
				peonsLeft++;
		}
		if (peonsLeft == 0)
			EndGame(EndGameCause.Win);
		else {
			int step = peonStartCount / musicLoops.Length;
			int capturedCount = peonStartCount - peonsLeft;
			int loopsToPlay = (capturedCount / step) + 1;
			for (int i = 0; i < loopsToPlay && i < musicLoops.Length; i++)
				musicLoops[i].mute = false;
		}
	}

	public void EndGame(EndGameCause cause) {
		Peon[] peons = FindObjectsOfType<Peon>();
		foreach (Peon p in peons)
			p.enabled = false;

		Player[] players = FindObjectsOfType<Player>();
		foreach (Player p in players)
			p.enabled = false;

		switch (cause) {
		case EndGameCause.Back:
			BackToMenu();
			break;

		case EndGameCause.Win:
			fadePanel.color = new Color(0, 0, 0, 0.75f);
			winPanel.SetActive(true);
			Player[] _players = new Player[2];
			foreach (Player p in players) {
				if (p.player == PlayerName.Player1)
					_players[0] = p;
				else
					_players[1] = p;
			}
			winText.text = (_players[0].score > _players[1].score) ? "Players 1 wins !" : "Players 2 wins !";
			Invoke("BackToMenu", 10);
			break;

		case EndGameCause.Timeout:
			fadePanel.color = new Color(0, 0, 0, 0.75f);
			timeoutPanel.SetActive(true);
			Invoke("BackToMenu", 10);
			break;
		}
	}

	public void BackToMenu() {
		SceneManager.LoadScene("Menu");
	}

	public int Compare (SortableElement x, SortableElement y) {
		if (x.transform.position.y > y.transform.position.y)
			return -1;
		else if (x.transform.position.y < y.transform.position.y)
			return 1;
		else
			return 0;
	}

	public Vector2 GetRandomConversionDestination() {
		int side = Random.Range(0, 4);
		switch (side) {
		default:
			return new Vector2(peonDestinationZone.min.x, Random.Range(peonDestinationZone.min.y, peonDestinationZone.max.y));
		case 1:
			return new Vector2(Random.Range(peonDestinationZone.min.x, peonDestinationZone.max.x), peonDestinationZone.min.y);
		case 2:
			return new Vector2(peonDestinationZone.max.x, Random.Range(peonDestinationZone.min.y, peonDestinationZone.max.y));
		case 3:
			return new Vector2(Random.Range(peonDestinationZone.min.x, peonDestinationZone.max.x), peonDestinationZone.max.y);
		}
	}

	void OnDrawGizmos() {
		Gizmos.matrix = transform.localToWorldMatrix;

		Gizmos.color = Color.yellow;
		Gizmos.DrawWireCube(peonStartZone.center, peonStartZone.size);

		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube(peonStartExclusionZone.center, peonStartExclusionZone.size);

		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(peonDestinationZone.center, peonDestinationZone.size);
	}

	public void PlaySentinelSound() {
		if (sentinelSoundPlaying) {
			Invoke("OnSentinelSoundEnd", sentinelSound.length);
			AudioSource.PlayClipAtPoint(sentinelSound, Camera.main.transform.position);
			sentinelSoundPlaying = true;
		}
	}

	void OnSentinelSoundEnd() {
		sentinelSoundPlaying = true;
	}

}
