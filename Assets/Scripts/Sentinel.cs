﻿using UnityEngine;
using System;

public class Sentinel : Peon
{
	[Header("Sentinels")]
	public float trackingSpeed = 0.25f;
	public float trackingRunSpeed = 1;
	public float trackingPause = 1;
	public float visionAngle = 90;
	public float visionDistance = 2;
	public Transform visionSprite;
	public float preachDetectionDistance = 2;

	private Player trackedPlayer = null;
	public bool IsTracking {
		get { return (trackedPlayer != null); }
	}

	private bool eventsAttached = false;

	protected override void Awake ()
	{
		base.Awake ();

		visionSprite.Rotate(Vector3.forward, visionAngle / 2, Space.Self);
		visionSprite.localScale = Vector3.one * visionDistance;
	}

	Peon Peon {
		get { return GetComponent<Peon>(); }
	}

	protected override void FixedUpdate ()
	{
		if (paused || listening || Converted || !IsTracking)
			base.FixedUpdate();
		else {
			Rigidbody2D body = GetComponent<Rigidbody2D>();

			Vector2 targetDirection = ((Vector2) trackedPlayer.transform.position - body.position).normalized;
			Vector3 cross = Vector3.Cross(direction, targetDirection);
			float angle = Vector2.Angle(direction, targetDirection);
			direction = Quaternion.AngleAxis(Mathf.Min(5, angle), cross) * direction;

			if (!paused) {
				float multiplier = trackingSpeed * Time.fixedDeltaTime;
				Vector3 newPos = (Vector3) (body.position + direction * multiplier);

				collider.enabled = false;
				bool found = false;
				float angleForMaxDistance = 0;
				if (Physics2D.OverlapPointNonAlloc(newPos, physicsResult, obstaclesLayerMask) > 0) {
					float f = lookupAngle;
					float maxDistance = float.MinValue;
					while (f < 180) {
						Vector2 newDir = Quaternion.AngleAxis(f, Vector3.forward) * direction;
						newPos = (Vector3) (body.position + newDir * multiplier);
						Vector2 end = (Vector2) newPos + newDir * 10000;
						if (Physics2D.LinecastNonAlloc(newPos, end, raycastResult, obstaclesLayerMask) == 0) {
							found = true;
							direction = newDir;
							break;
						}
						else {
							if (maxDistance < raycastResult[0].distance) {
								maxDistance = raycastResult[0].distance;
								angleForMaxDistance = f;
							}
						}

						newDir = Quaternion.AngleAxis(-f, Vector3.forward) * direction;
						newPos = (Vector3) (body.position + newDir * multiplier);
						end = (Vector2) newPos + newDir * 10000;
						if (Physics2D.LinecastNonAlloc(newPos, end, raycastResult, obstaclesLayerMask) == 0) {
							found = true;
							direction = newDir;
							break;
						}
						else {
							if (maxDistance < raycastResult[0].distance) {
								maxDistance = raycastResult[0].distance;
								angleForMaxDistance = f;
							}
						}

						f += lookupAngle;
					}

					if (!found) {
						direction = Quaternion.AngleAxis(angleForMaxDistance, Vector3.forward) * direction;
						newPos = (Vector3) (body.position + direction * multiplier);
						found = true;
					}
				}
				else
					found = true;

				if (found)
					body.MovePosition(newPos);
				collider.enabled = true;
			}
		}
	}

	protected override void Update ()
	{
		base.Update ();

		if (!Converted && !listening) {
			visionSprite.gameObject.SetActive (IsTracking);

			float angle = Vector2.Angle (Vector2.up, direction);
			visionSprite.rotation = Quaternion.identity;
			if (direction.x > 0)
				visionSprite.Rotate (Vector3.forward, -angle + visionAngle / 2, Space.Self);
			else
				visionSprite.Rotate (Vector3.forward, angle + visionAngle / 2, Space.Self);

			Player[] players = FindObjectsOfType<Player> ();

			if (!IsTracking || trackedPlayer.IsPreaching == false) {
				Player nearestPreachingPlayer = null;
				float nearestDistance = float.MaxValue;
				foreach (Player p in players) {
					if (p.IsPreaching) {
						float distance = Vector2.Distance (transform.position, p.transform.position);
						if (distance < preachDetectionDistance && distance < nearestDistance) {
							nearestPreachingPlayer = p;
							nearestDistance = distance;
						}
					}
				}

				if (nearestPreachingPlayer != null)
					Track (nearestPreachingPlayer);
			}

			if (IsTracking) {
				foreach (Player p in players) {
					float dist = Vector2.Distance(transform.position, p.transform.position);
					if (dist < this.collider.radius) {
						if (p.IsPreaching)
							p.EndPreach(false);
						else
							p.Stun();
					}
				}
			}

			Animator anim = GetComponent<Animator> ();
			anim.SetBool ("Aware", IsTracking);
		}
		else
			visionSprite.gameObject.SetActive(false);
	}

	public void Track(Player tracked) {
		if (IsTracking == false || trackedPlayer.IsPreaching == false) {
			CancelInvoke();

			if (trackedPlayer != tracked || !eventsAttached) {
				trackedPlayer = tracked;
				trackedPlayer.PreachEnded += OnPreachEnded;
				eventsAttached = true;
			}

			paused = true;
			Invoke("Unpause", trackingPause);

			FindObjectOfType<GameplayManager>().PlaySentinelSound();
		}
	}

	void Unpause() {
		paused = false;
	}

	void OnPreachEnded(Player sender, bool success) {
		sender.PreachEnded -= OnPreachEnded;
		eventsAttached = false;
		Invoke("EndTracking", 2);
	}

	void EndTracking() {
		trackedPlayer = null;
	}

	protected override void OnDrawGizmos ()
	{
		base.OnDrawGizmos ();

		Gizmos.color = Color.red;
		Gizmos.DrawSphere(Vector3.zero, 0.1f);

		if (IsTracking) {
			Gizmos.color = Color.white;
			Gizmos.DrawLine(Vector3.zero, Quaternion.AngleAxis(visionAngle / 2, Vector3.forward) * direction);
			Gizmos.DrawLine(Vector3.zero, Quaternion.AngleAxis(-visionAngle / 2, Vector3.forward) * direction);

			Gizmos.color = Color.blue;
			Gizmos.DrawLine(Vector3.zero, transform.InverseTransformPoint(trackedPlayer.transform.position));
		}
	}

}
