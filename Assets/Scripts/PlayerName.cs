﻿using UnityEngine;

public enum PlayerName {
	Player1, Player2
}

public static class PlayerNameExtensions {

	public static float GetAxis(this PlayerName player, RectTransform.Axis axis) {
		InputType type = InputConfigurator.InputTypeForPlayer(player);

		if (type.IsPad()) {
			string an = string.Format("{0} ({1})", axis, player);
			return Input.GetAxis(an);
		}

		else if (axis == RectTransform.Axis.Horizontal) {
			float value = 0;
			if (Input.GetKey(InputConfigurator.KeyForPlayer(player, KeyName.Left)))
				value -= 1;
			if (Input.GetKey(InputConfigurator.KeyForPlayer(player, KeyName.Right)))
				value += 1;
			return value;
		}

		else {
			float value = 0;
			if (Input.GetKey(InputConfigurator.KeyForPlayer(player, KeyName.Up)))
				value += 1;
			if (Input.GetKey(InputConfigurator.KeyForPlayer(player, KeyName.Down)))
				value -= 1;
			return value;
		}
	}

	public static bool GetKeyUp(this PlayerName player, KeyName key) {
		return Input.GetKeyUp(InputConfigurator.KeyForPlayer(player, key));
	}

}