﻿public enum InputType {
	KeyboardAzerty, KeyboardQwerty, PadPlaystation, PadXbox, Unknown
}

public static class InputTypeExtensions {

	public static bool IsPad(this InputType type) {
		return (type == InputType.PadPlaystation || type == InputType.PadXbox);
	}

}