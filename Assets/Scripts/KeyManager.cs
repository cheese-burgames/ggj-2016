﻿using UnityEngine;
using System;

public class KeyManager : MonoBehaviour {

	[Serializable]
	public class KeyDescriptor {
		public KeyName name;
		public Sprite player1Sprite; // Keyboard
		public Sprite player2Sprite; // Keyboard
		public Sprite psSprite;
		public Sprite xboxSprite;
	}

	public KeyDescriptor[] keyDescriptors;

	public static Sprite GetKeySpriteForPlayer(KeyName name, PlayerName player) {
		KeyManager km = FindObjectOfType<KeyManager>();
		if (km != null) {
			foreach (KeyDescriptor kd in km.keyDescriptors) {
				if (kd.name == name) {
					InputType type = InputConfigurator.InputTypeForPlayer(player);
					switch (type) {
					case InputType.KeyboardAzerty:
					case InputType.KeyboardQwerty:
						return (player == PlayerName.Player1) ? kd.player1Sprite : kd.player2Sprite;
					case InputType.PadPlaystation:
						return kd.psSprite;
					case InputType.PadXbox:
						return kd.xboxSprite;
					}
				}
			}
		}
		return null;
	}

}
