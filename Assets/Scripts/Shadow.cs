﻿using UnityEngine;
using System.Collections;

public class Shadow : MonoBehaviour {

	public Transform trackedObject;

	void Update () {
		transform.position = trackedObject.transform.position;
	}

}
