﻿using UnityEngine;

public class ArcPainter {

	private static Vector2[] circleVertices = null;

	private static void BuildVertices() {
		if (circleVertices == null)
		{
			circleVertices = new Vector2[360];
			
			for (int i = 0; i < 360; i++)
			{
				float angle = Mathf.Deg2Rad * i;
				circleVertices[i] = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
			}
		}
	}

	public static void DrawArc(Matrix4x4 matrix, float radius, int startAngle, int endAngle, Color color) {
		BuildVertices();

		startAngle %= 360;
		endAngle %= 360;

		if (startAngle == endAngle)
		{
			if (startAngle == 0)
				endAngle = 360;
			else
				return;
		}

		GL.PushMatrix();
		GL.MultMatrix(matrix);
		GL.Begin( GL.LINES );
		GL.Color(color);
		
		int sa = Mathf.Min (startAngle, endAngle);
		endAngle = Mathf.Max (startAngle, endAngle);
		startAngle = sa;

		Vector2 v = circleVertices[(startAngle < 0) ? (startAngle + 360) : startAngle];
		GL.Vertex3(v.x * radius, v.y * radius, 0);
		for (int i = startAngle + 1; i < endAngle - 1; i++)
		{
			v = circleVertices[(i < 0) ? (i + 360) : i];
			
			float x = v.x * radius;
			float y = v.y * radius;
			GL.Vertex3( x, y, 0 );
			GL.Vertex3( x, y, 0 );
		}
		v = circleVertices[(endAngle == 360) ? 0 : endAngle];
		GL.Vertex3(v.x * radius, v.y * radius, 0);
		
		GL.End();
		GL.PopMatrix();
	}

	public static void DrawThickArc(Matrix4x4 matrix, float innerRadius, float outerRadius, int startAngle, int endAngle, Color color) {
		BuildVertices();

		if (Mathf.Approximately(innerRadius, outerRadius))
		{
			DrawArc(matrix, innerRadius, startAngle, endAngle, color);
			return;
		}

		startAngle %= 360;
		endAngle %= 360;

		if (startAngle == endAngle)
		{
			if (startAngle == 0)
				endAngle = 360;
			else
				return;
		}

		float ir = Mathf.Min (innerRadius, outerRadius);
		outerRadius = Mathf.Max (innerRadius, outerRadius);
		innerRadius = ir;
		
		GL.PushMatrix();
		GL.MultMatrix(matrix);
		GL.Begin(GL.TRIANGLE_STRIP);
		GL.Color(color);
		
		int sa = Mathf.Min(startAngle, endAngle);
		endAngle = Mathf.Max (startAngle, endAngle);
		startAngle = sa;

		Vector2 v = circleVertices[(startAngle + 360) % 360];
		GL.Vertex3(v.x * outerRadius, v.y * outerRadius, 0);
		GL.Vertex3(v.x * innerRadius, v.y * innerRadius, 0);
		for (int i = startAngle + 1; i <= endAngle; i++) {
			v = circleVertices[(i + 360) % 360];

			GL.Vertex3(v.x * outerRadius, v.y * outerRadius, 0);
			GL.Vertex3(v.x * innerRadius, v.y * innerRadius, 0);
		}

		GL.End();
		GL.PopMatrix();
	}

	public static Mesh BuildArcMesh(float radius, int startAngle, int endAngle, Color color, float minimumAlpha = 0) {
		BuildVertices();
		
		startAngle %= 360;
		endAngle %= 360;
		
		if (startAngle == endAngle)
		{
			if (startAngle == 0)
				endAngle = 360;
			else
				return new Mesh();
		}
		
		int sa = Mathf.Min (startAngle, endAngle);
		endAngle = Mathf.Max (startAngle, endAngle);
		startAngle = sa;

		int nbTriangles = Mathf.Abs(endAngle - startAngle);
		int nbVertices = nbTriangles + 2;

		Mesh mesh = new Mesh();
		mesh.MarkDynamic();

		Vector3[] vertices;
		Vector2[] uvs;
		Color[] colors;
		int[] triangles;

		vertices = new Vector3[nbVertices];
		uvs = new Vector2[nbVertices];
		colors = new Color[nbVertices];
		triangles = new int[nbTriangles * 3];

		vertices[0] = Vector3.zero;
		uvs[0] = Vector2.zero;

		colors[0] = color;
		Color outerColor = new Color(color.r, color.g, color.b, minimumAlpha);
		float uvIncrement = Mathf.Min(0.1f, 1.0f / nbTriangles);
		for (int i = 1; i < colors.Length; i++) {
			colors[i] = outerColor;
			uvs[i] = new Vector2(1, uvIncrement * (i - 1));
		}

		for (int i = 0, offset = 0; i < nbTriangles; i++)
		{
			triangles[offset++] = 0;
			triangles[offset++] = i + 1;
			triangles[offset++] = i + 2;
		}

		int vi = 1;
		for (int i = startAngle; i < endAngle; i++)
		{
			Vector2 v = circleVertices[(i < 0) ? (i + 360) : i];
			vertices[vi++] = new Vector3(v.x * radius, v.y * radius, 0);
		}

		mesh.vertices = vertices;
		mesh.uv = uvs;
		mesh.colors = colors;
		mesh.triangles = triangles;

		mesh.Optimize();

		return mesh;
	}

}
